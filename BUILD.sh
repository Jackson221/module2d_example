echo "Release mode build"
export CC=gcc
export CXX=/usr/bin/g++-10

mkdir log
mkdir build
cd build

rm CMakeCache.txt
cmake .. -DRELEASE=on
echo ------------------------------------------------------------------------------------------------------------
make -j$(nproc)
echo ------------------------------------------------------------------------------------------------------------
cd ../module2d/src/shaders
./shadercompile.sh
cd ../../../
