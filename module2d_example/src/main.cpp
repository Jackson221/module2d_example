#include "engine.hpp"
#include "UI/UI.hpp"
#include "input/computer.hpp"
#include "filesystem/filesystem.hpp"
#include "render/sprite.hpp"
#include "render/object.hpp"
#include "render/game_render.hpp"
#include "isOn.h"
#include "simulation/physics.hpp"

#include "container/idmap.hpp"
#include "test/test.hpp"


std::string game_title = "Module2D Example";

sim::physen* global_physics_en;
render::render_engine* global_render_en;

class draggable_box : public ui::element,
	public ui::has_event<input::computer::mouse_button::event_data_t>,
	public ui::has_event<input::computer::mouse_motion::event_data_t>
{
	ui::rectangle rect;
	bool m1_down = false;
	public:
		draggable_box(ui::state uistate, ui::color color) : 
			ui::element(uistate),
			rect(uistate, color)
		{
			add_child(rect);
			rect.positioning_rectangle.set_origin_size_calculator([this]()
			{
				return positioning_rectangle.get_size();
			});
		}
		void impl_dispatch_event(input::computer::mouse_button::event_data_t button) override
		{
			m1_down = button.button == input::computer::mouse_button::button_t::left? button.is_down : m1_down;
		}
		void impl_dispatch_event(input::computer::mouse_motion::event_data_t motion) override
		{
			if (m1_down)
			{
				positioning_rectangle.set_position(positioning_rectangle.get_position() + 
						mm::vec2{static_cast<double>(motion.delta_x),static_cast<double>(motion.delta_y)});
			}
		}
};
/*std::vector<render::object_tracker> render_objects;
void create_example_object(render::state* rstate, mm::vec2r pos, mm::vec2r vel)
{
	auto size = mm::vec2{25,25};
	auto obj = new sim::object(*global_physics_en, pos,vel,15.0);
	obj->collision_info.addCollisionRectangle({0,0,0,size.x,size.y});
	size_t render_it = render_objects.size();
	render_objects.emplace_back(*global_render_en, rstate, "test_square", mm::vec2r{0,0,0}, size, mm::rect{0,0,9,9});
	global_physics_en->add_tick_finished_callback([obj,render_it]()
	{
		render_objects[render_it]->position = obj->position;
	});
}	*/
void create_example_object(render::state* rstate, mm::vec2r pos, mm::vec2r vel) {}

class module2d_example : public game_base
{
	public:
		module2d_example() : 
			game_base("Module2D Example")
		{
		}
	int game_main(int argc, char* argv[], engine_state & estate) override
	{
		ui::state uistate = estate.uistate;
		ui::window_manager* wm = estate.wm;

		GE_Font_LoadFromDir(filesystem::concat_directories(filesystem::app_root_directory,"module2d_example/fonts"));
		render::load_all_sprites_in_dir(*estate.rstate, filesystem::concat_directories(filesystem::app_root_directory,"module2d_example/sprites"));


		/////////////////////Styling/////////////////////
		auto small_sans_font = GE_Font_GetFont("FreeSans",15).value();
		ui::color window_background_color_focused = ui::color{0x66,0xff,0x33,0xff};
		ui::color window_background_color_unfocused = ui::color{0xa4,0xf4,0x88};
		auto my_menu_list_style = ui::menu_list_style
		{
			{ui::color{0x00,0x00,0x00,0xff},"FreeSans",35}, //font
			window_background_color_focused, //background
			window_background_color_unfocused, //background
			ui::color{0xff,0x00,0x00,255}, //highlight

			ui::color{0x00,0x22,0x00,0xff}, //spacer color
			4.0, //spacer size
			0.9 //coeffecient spacer width
		};
		auto window_title_font = ui::font_style_t{ui::color{0x00,0x00,0x00,0xff},"FreeSans",18};
		auto window_title_style = ui::window_title_style_t{window_title_font,0,{ui::color{0xff,0x00,0x00,0xff},ui::color{0x66,0x66,0x66,0xff},{0x33,0x33,0x33,0xff}},window_title_font,2.5,2,ui::color{0x66,0xff,0x33,0xff},25,true};
		auto window_style = ui::window_style_t{window_title_style, ui::color{0x00,0x0a,0x00,0xff},2,window_background_color_focused, window_background_color_unfocused};
		auto main_style = ui::style{window_title_font,{ui::color{0x00,0x33,0x00,0xff},"FreeSans",18},window_style};

		auto popup_style = ui::popup_ok_style_t{};
		popup_style.window = main_style;
		popup_style.buttonFont = ui::font_style_t{ui::color{0xff,0xff,0xff,0xff},"FreeSans",20};
		popup_style.buttons = ui::button_style_t{ui::color{0x00,0x33,0x00,255},ui::color{0x55,0xee,0x22,0xff},ui::color{0x1e,0x88,0x1a,0xff}};
		popup_style.buttonSize = mm::vec2{10,2};
		popup_style.useSizeAsPadding = true;
		popup_style.buttonSpacing = 15;
		popup_style.buttonDistanceFromBottom = 5;

		/////////////////////Widgets setup/////////////////////


		auto my_draggable_progress_bar_example = ui::draggable_progress_bar(uistate,{0xff,0x00,0x00,0x00},{0x00,0x33,0x00,0xff},false);
		my_draggable_progress_bar_example.positioning_rectangle.set_position({100,100});
		my_draggable_progress_bar_example.positioning_rectangle.set_size({100,50});

		auto painting_boundries_example_boundry = draggable_box(uistate, {0x00,0xff,0x00,0xff});
		painting_boundries_example_boundry.positioning_rectangle.set_size({150,150});
		painting_boundries_example_boundry.positioning_rectangle.set_position({200,200});

		auto painting_boundries_test_text = ui::text(uistate, "Drag Rectangle Drag Rectangle Drag Rectangle Drag Rectangle", ui::color{0xff,0x00,0x00,0xff}, small_sans_font);
		painting_boundries_test_text.painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(painting_boundries_example_boundry.positioning_rectangle);
		//painting_boundries_test_text.positioning_rectangle.set_size({100,100});
		painting_boundries_test_text.positioning_rectangle.set_modifier(ui::positioning_rectangle_t::modifiers::expand_size_to_fit,true);
		painting_boundries_test_text.positioning_rectangle.set_position({200,200});
		painting_boundries_example_boundry.add_child_without_modfying_positioning_rectangles(painting_boundries_test_text);



		auto text_left_boundry = ui::rectangle(uistate, {0x00,0x00,0xff,0xff});
		text_left_boundry.positioning_rectangle.set_size({2,2});
		text_left_boundry.positioning_rectangle.set_position(painting_boundries_test_text.positioning_rectangle.get_position() - mm::vec2{2,2});
		painting_boundries_example_boundry.add_child_without_modfying_positioning_rectangles(text_left_boundry);
		auto text_right_boundry = ui::rectangle(uistate, {0x00,0x00,0xff,0xff});
		text_right_boundry.positioning_rectangle.set_size({2,2});
		text_right_boundry.positioning_rectangle.set_position(painting_boundries_test_text.positioning_rectangle.get_position()+painting_boundries_test_text.positioning_rectangle.get_size() - mm::vec2{2,2});
		painting_boundries_example_boundry.add_child_without_modfying_positioning_rectangles(text_right_boundry);

		auto null_callback = [](){printf("Clicked an item.\n");};

		auto menu_tuple = std::tuple
		{
			ui::regular_menu_item_descriptor{"Direct action",null_callback},
			ui::divider_menu_item_descriptor{},
			ui::regular_menu_item_descriptor{"Another action",null_callback},
			ui::submenu_menu_item_descriptor{"Horixontal Submenu", std::tuple
			{
				ui::regular_menu_item_descriptor{"This can be used",null_callback},
				ui::divider_menu_item_descriptor{},
				ui::regular_menu_item_descriptor{"In a file menu",null_callback},
				ui::submenu_menu_item_descriptor{"Too!", std::tuple
				{
					ui::regular_menu_item_descriptor{"Cool.",null_callback},
				},true}
			},false},
			ui::submenu_menu_item_descriptor{"Normal submenu", std::tuple
			{
				ui::regular_menu_item_descriptor{"Yup",null_callback},
				ui::regular_menu_item_descriptor{"This works too",null_callback}
			},true}
		};
		
		auto example_menu_list_button = ui::menu_list_button(uistate,my_menu_list_style,{300,65},"Drop-Down Button", true, menu_tuple);
		example_menu_list_button.positioning_rectangle.set_position({500,100});
		example_menu_list_button.painting_boundries_rectangle.set_origin_position_and_size_to_be_always_same_as(wm->painting_boundries_rectangle);


		wm->add_window(&painting_boundries_example_boundry);
		wm->add_window(&my_draggable_progress_bar_example);
		wm->add_window(&example_menu_list_button);


		
		//ui::popup_message(uistate, wm, wm->positioning_rectangle.get_size()/2, {550,120},"Module2D Example", "Currently only demos the UI system. More to come later.",popup_style.buttonFont, popup_style);

		render::camera camera;
		camera.screen = wm->positioning_rectangle.get_size();


#if DEBUG_RENDERS_ENABLED()
		debug::pass_uistate(uistate,&camera);
#endif
		/////////////////////Create physics engine/////////////////////
		
		auto physen = sim::physen{};

		global_physics_en = &physen;

		std::optional<render::render_engine> render_engine_storage;


		/////////////////////Create render engine & objects/////////////////////
		{
			auto _ = std::lock_guard(physen.physics_engine_mutex);

			render_engine_storage.emplace(physen);
			global_render_en = &render_engine_storage.value();

			create_example_object(uistate.rstate,{0,3,0},{50,0,0});
			create_example_object(uistate.rstate,{100,0,0},{0,0,0});
		}
		/////////////////////Set background/////////////////////
		//wm->set_background(std::make_unique<ui::rectangle>(uistate,ui::color{0x00,0x15,0x00,0xff}));
		wm->set_background(std::make_unique<ui::game_render>(uistate,  *global_render_en, &camera));
		/////////////////////Main loop/////////////////////

		std::this_thread::sleep_for(std::chrono::milliseconds(30)); //TODO: Work around race conditions in render/object.cpp will fix when I refactor that file
		//auto _ = std::lock_guard(physics_engine_mutex);
		while (GE_IsOn)
		{
			//camera.pos.r += 0.01;
			//camera.pos.x += 1;
			estate.rstate->api.begin_frame();

			{
				auto _ = std::lock_guard(uistate.globallock->lock);
				wm->render();
#if DEBUG_RENDERS_ENABLED()
				physics_debug_instance->render();
				debug::this_thread_instance.done_drawing();
				debug::this_thread_instance.render();
				debug::render_sticky_objects();
#endif
			}

			estate.rstate->api.render();
		}


		{
			auto _ = std::lock_guard(physen.physics_engine_mutex);
			//render_objects.clear();
		}
		

		return 0;
	}
} module2d_example_instance;
game_base* game = &module2d_example_instance;
